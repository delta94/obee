# React Native Typescript + Redux Toolkit + Translation + Theme

## Installation

Before installation of this project, make sure that you have already set up your environment to run [React Native](https://reactnative.dev/docs/environment-setup). 

1. Fork this repo
```
git clone https://github.com/captainbuggythefifth/ReactNativeReduxToolkit.git YourNewProjectFolder
```
2. You can rename this project to whatever you want it using [React Native Rename](https://www.npmjs.com/package/react-native-rename)

```
cd YourNewProjectFolder/
yarn global add react-native-rename
react-native-rename YourNewProjectName
```

3. Install dependencies. I would advise to use [Yarn](https://yarnpkg.com/) to install the dependencies.


```bash
yarn
```

4. Install Pods
```
cd ios/ && pod install && cd ..
```

5. Start the application!
```
npx react-native start
```

6. Open a new start and run the application!
```
npx react-native run-ios
```
And/or
```
npx react-native run-android
```

😁
