export const EN = "en";
export const VI = "vi";
export const DEFAULT = VI;

export const EN_LONG = "ENGLISH";
export const VI_LONG = "VIETNAM";

export const PERSISTENCE_KEY = "LanguagePersistenceKey";

export interface IAvailableLabelsValues {
    label: string,
    value: string
}

export const AVAILABLE_LABELS_VALUES: IAvailableLabelsValues[] = [
    {
        label: EN_LONG,
        value: EN
    },
    {
        label: VI_LONG,
        value: VI
    }
]